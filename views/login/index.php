<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<form method="post">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2 align="center">Login Form</h2>
				</div>
				
				<div class="panel-body">
					<label>Username</label>
					<input type="text" name="uname" class="form-control">
					<label>Password</label>
					<input type="password" name="pass" class="form-control">
				</div>
				<div class="panel-footer">
					<button class="btn btn-block btn-primary"><i class="fa fa-lg fa-sign-in"></i> Login</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>