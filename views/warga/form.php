<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<?php require("views/layouts/admin-menu.php"); ?>
		</div>
		<div class="col-sm-9">
			<div class="panel">
				<div class="panel-body">
					<h2>Data Warga</h2>
					<form method="post">
						<label>NIK</label>
						<input type="text" name="NIK" class="form-control" value="<?=@$model->NIK;?>">
						<label>Nama</label>
						<input type="text" name="nama" class="form-control" value="<?=@$model->nama;?>">
						<label>Alamat</label>
						<textarea name="alamat" class="form-control"><?=@$model->alamat;?></textarea>
						<label>Jenis Kelamin</label>
						<select name="jenis_kelamin" required="" class="form-control">
							<option value="">-Pilih-</option>
							<option value="Laki-laki">Laki-laki</option>
							<option value="Perempuan">Perempuan</option>
						</select>
						<label>No Telepon</label>
						<input type="text" name="no_telepon" class="form-control" value="<?=@$model->no_telepon;?>">
						<p></p>
						<button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>