<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<?php require("views/layouts/admin-menu.php"); ?>
		</div>
		<div class="col-sm-9">
			<div class="panel">
				<div class="panel-body">
					<h2>Data Warga</h2>
					<button class="btn btn-success" onclick="location='<?=URL;?>/warga/tambah'"><i class="fa fa-plus"></i> Tambah</button>
					<p></p>
					<table class="table table-bordered">
						<tr>
							<th>No</th>
							<th>NIK</th>
							<th>Nama</th>
							<th>Alamat</th>
							<th>Jenis Kelamin</th>
							<th>No Telepon</th>
							<th>Aksi</th>
						</tr>
						<?php
						$no=0;
						if($model->length){
							foreach ($model->data as $val) {
						?>
						<tr>
							<td><?=++$no;?></td>
							<td><?=$val->NIK;?></td>
							<td><?=$val->nama;?></td>
							<td><?=$val->alamat;?></td>
							<td><?=$val->jenis_kelamin;?></td>
							<td><?=$val->no_telepon;?></td>
							<td>
								<button onclick="location='<?=URL;?>/warga/edit?NIK=<?=$val->NIK;?>'" class="btn btn-warning"><i class="fa fa-pencil"></i></button>
								<button onclick="location='<?=URL;?>/warga/hapus?NIK=<?=$val->NIK;?>'" class="btn btn-danger"><i class="fa fa-trash"></i></button>
							</td>
						</tr>
						<?php
							}
						}else{
						?>
						<tr>
							<td colspan="7"><center>Tidak Ada Data</center></td>
						</tr>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>