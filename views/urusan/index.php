<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<?php require("views/layouts/admin-menu.php"); ?>
		</div>
		<div class="col-sm-9">
			<div class="panel">
				<div class="panel-body">
					<h2>Data Urusan</h2>
					<button class="btn btn-success" onclick="location='<?=URL;?>/urusan/tambah'"><i class="fa fa-plus"></i> Tambah</button>
					<p></p>
					<table class="table table-bordered">
						<tr>
							<th>No</th>
							<th>NIK</th>
							<th>Nama</th>
							<th>Urusan</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
						<?php
						$no=0;
						if($model->length){
							foreach ($model->data as $val) {
								$status = ($val->status == 1) ? "Belum Selesai" : (($val->status==2) ? "Selesai" : "Selesai dan Terkirim");
								$wrg = $warga($val->NIK);
						?>
						<tr>
							<td><?=++$no;?></td>
							<td><?=$val->NIK;?></td>
							<td><?=$wrg->nama;?></td>
							<td><?=$val->jenis_urusan;?></td>
							<td><?=$status;?></td>
							<td>
								<?php if($val->status==1){ ?>
								<button onclick="location='<?=URL;?>/urusan/selesai?kd_urusan=<?=$val->kd_urusan;?>'" class="btn btn-warning">Selesai Dan Kirim Pemberitahuan</button>
								<?php }else echo "-"; ?>
							</td>
						</tr>
						<?php
							}
						}else{
						?>
						<tr>
							<td colspan="6"><center>Tidak Ada Data</center></td>
						</tr>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>