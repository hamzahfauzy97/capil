<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<?php require("views/layouts/admin-menu.php"); ?>
		</div>
		<div class="col-sm-9">
			<div class="panel">
				<div class="panel-body">
					<h2>Data Urusan</h2>
					<form method="post">
						<input type="hidden" name="kd_urusan" value="<?=@$model->kd_urusan;?>">
						<label>NIK</label>
						<input id="NIK" type="text" name="NIK" class="form-control" value="<?=@$model->NIK;?>">
						<p id="nama"></p>
						<label>Urusan</label>
						<select name="jenis_urusan" required="" class="form-control">
							<option value="">-Pilih-</option>
							<option value="KTP">KTP</option>
							<option value="KK">KK</option>
							<option value="AKTA">AKTA</option>
						</select>
						<p></p>
						<button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$("#NIK").blur(function(){
	$.get("<?=URL;?>/urusan/get-warga?NIK="+$(this).val(),function(response){
		if(response == "0")
			$("#nama").html("Data tidak ditemukan")
		else 
			$("#nama").html(response);
	});
});
</script>