<?php
namespace controllers;

use libs\Controller;
use libs\Session;
use models\Warga_Model;
use models\Urusan_Model;

class Urusan extends Controller
{

	function __construct(){
		parent::__construct();
		Session::init();
		$this->view->title = "Data Urusan";
	}
	
	function actionIndex(){
		if(!Session::get("username"))
			$this->redirect();
		$model = new Urusan_Model();
		$warga = function($NIK){
			$model = new Warga_Model;
			$model->find()->where(["NIK"=>$NIK])->one();
			return $model;
		};
		$model->find()->execute();
		return $this->view->render("index",1,["model"=>$model,"warga"=>$warga]);
	}

	function actionTambah(){
		if(!Session::get("username"))
			$this->redirect();
		$model = new Urusan_Model;
		if($this->request("POST")){
			$model->attr($_POST);
			$model->status = 1;
			$model->tanggal = "CURRENT_TIMESTAMP";
			if($model->save())
				return $this->redirect("urusan");
		}
		return $this->view->render("form",1);
	}

	function actionSelesai($kd_urusan){
		if(!Session::get("username"))
			$this->redirect();
		$model = new Urusan_Model();
		$model->find()->where(["kd_urusan"=>$kd_urusan])->one();
		$model->status = 2;
		$model->tanggal = "CURRENT_TIMESTAMP";
		if($model->save())
			return $this->redirect("urusan");
	}
	function actionTerkirim($kd_urusan){
		$model = new Urusan_Model();
		$model->find()->where(["kd_urusan"=>$kd_urusan])->one();
		$model->status = 3;
		$model->tanggal = "CURRENT_TIMESTAMP";
		$model->save();
	}

	function actionGetUrusan(){
		$model = new Urusan_Model();
		$model->find()->where(["status"=>2])->execute();
		if($model->length){
			$response = [];
			foreach ($model->data as $val) {
				$val = (array) $val;
				$val['no_telepon'] = $this->getHp($val['NIK'])->no_telepon;
				$val['nama'] = $this->getHp($val['NIK'])->nama;
				$response["urusan"][] = $val;
			}
			echo json_encode($response);
		}else{
			echo 0;
		}
	}

	function actionGetWarga($NIK){
		$model = new Warga_Model;
		$model->find()->where(["NIK"=>$NIK])->one();
		if($model->length)
			echo $model->nama;
		else
			echo 0;
	}

	function GetHp($NIK){
		$model = new Warga_Model;
		$model->find()->where(["NIK"=>$NIK])->one();
		if($model->length)
			return $model;
		else
			echo 0;
	}

	function actionGateway(){
		return $this->view->render("gateway",0);
	}

}