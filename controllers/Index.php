<?php

namespace controllers;
use libs\Controller;
use libs\Session;

class Index extends Controller {

	function __construct(){
		parent::__construct();
		Session::init();
		if(!Session::get("username"))
			$this->redirect("login");
		$this->view->title = "Dinas Catatan Sipil Batubara";
	}

	function actionIndex(){
		return $this->view->render("index",1);
	}

	function actionLogout(){
		Session::destroy();
		$this->redirect();
	}
	

}