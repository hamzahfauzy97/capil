<?php
namespace controllers;

use libs\Controller;
use libs\Session;
use models\Warga_Model;

class Warga extends Controller
{

	function __construct(){
		parent::__construct();
		Session::init();
		if(!Session::get("username"))
			$this->redirect();
		$this->view->title = "Data Warga";
	}
	
	function actionIndex(){
		$model = new Warga_Model();
		$model->find()->execute();
		return $this->view->render("index",1,["model"=>$model]);
	}

	function actionTambah(){
		$model = new Warga_Model();
		if($this->request("POST")){
			$model->attr($_POST);
			if($model->save())
				return $this->redirect("warga");
		}
		return $this->view->render("form",1);
	}

	function actionEdit($NIK){
		$model = new Warga_Model();
		if($this->request("POST")){
			$model->attr($_POST);
			if($model->save())
				return $this->redirect("warga");
		}
		$model->find()->where(['NIK'=>$NIK])->one();
		return $this->view->render("form",1,["model"=>$model]);
	}

	function actionHapus($NIK){
		$model = new Warga_Model();
		$model->delete(["NIK"=>$NIK]);
		return $this->redirect("warga");
	}

}